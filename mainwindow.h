#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QDate>

namespace Ui {
class MainWindow;
}

// NOTE: I know, that for clearness it is better to separate this classes in different files.
// But by readability reasons they stay here.

class DateHelper {  // Static class with helping functions
public:
    static QString getDayOfWeekLocalizedName(int dayOfWeekNum);  // Localized name of day of week. Input [1, 7]
    static int getDaysCountInMonth(int month, bool isLeapYear);  // Return number of days in month
    static bool isLeapYear(int year);  // Checks if year is leap
};

struct DayOfYear {  // Struct stores day of year value (without exact year)
    int day;
    int month;
    bool isLeapYear;

    int daysFromStartOfYear();  // This function return count of days since year start
    int daysTillEndOfYear();  // This function return count of days till year end
    // Sum of this two functions +1 for same date must give 365(366) days a year.
};

struct Date {  // Small date struct. Just for convenience.
    int day;
    int month;
    int year;

    // Constructors
    Date(int day, int month, int year);
    Date(QDate fromDate);
    DayOfYear toDayOfYear();  // Cast this struct to DayOfYear struct
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    // UI functions
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void calculateDate(int day, int month, int year); // Count target day of week relative to todays date.

    int getDaysDistance(Date fromDate, Date toDate);  // Return distance in days between 2 dates

private slots:
    void updateDayOfWeek();  // Update ui function
    void updateMaxMonthDays();  // Set limits in ui for max count of days in month.

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
