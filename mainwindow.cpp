#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->day->setValue(QDate::currentDate().day());
    ui->month->setValue(QDate::currentDate().month());
    ui->year->setValue(QDate::currentDate().year());

    connect(ui->day,   SIGNAL(valueChanged(int)), SLOT(updateDayOfWeek()));
    connect(ui->month, SIGNAL(valueChanged(int)), SLOT(updateDayOfWeek()));
    connect(ui->year,  SIGNAL(valueChanged(int)), SLOT(updateDayOfWeek()));

    connect(ui->month, SIGNAL(valueChanged(int)), SLOT(updateMaxMonthDays()));
    connect(ui->year,  SIGNAL(valueChanged(int)), SLOT(updateMaxMonthDays()));

    updateMaxMonthDays();
    updateDayOfWeek();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDayOfWeek()
{
    calculateDate(ui->day->value(),ui->month->value(),ui->year->value());
}

void MainWindow::updateMaxMonthDays()
{
    if (ui->year->value() == 0) {  // Try to ignore zero year.
        ui->year->setValue(-1);
        return;
    }
    auto month = ui->month->value();
    auto isLeapYear = DateHelper::isLeapYear(ui->year->value());
    ui->day->setMaximum(DateHelper::getDaysCountInMonth(month, isLeapYear));
}

void MainWindow::calculateDate(int day, int month, int year)
{
    auto currentDayOfWeek = QDate::currentDate().dayOfWeek();

    Date currentDate(QDate::currentDate());
    Date targetDate(day, month, year);

    auto distance = getDaysDistance(targetDate, currentDate);
    currentDayOfWeek--;  // Move to range [0, 6]
    auto targetDayOfWeek = currentDayOfWeek - distance%7;  // Add days distance. Result value may be nagative
    targetDayOfWeek = (7 + targetDayOfWeek)%7;  // Move negative side to positive and trim to 7;
    targetDayOfWeek++;  // Move back to range [1, 7]
    // Set localized dayOfWeek to ui
    ui->dayOfWeek->setText(DateHelper::getDayOfWeekLocalizedName(targetDayOfWeek));
}

int MainWindow::getDaysDistance(Date fromDate, Date toDate)
{
    // If same year - count different way
    if (fromDate.year == toDate.year) {
        auto lessDate = fromDate.toDayOfYear().daysFromStartOfYear();
        auto moreDate = toDate.toDayOfYear().daysFromStartOfYear();
        return moreDate - lessDate;
    }

    // Count days till end and start of year in dates tails.
    auto daysBetween = 1;
    daysBetween += fromDate.toDayOfYear().daysTillEndOfYear();
    daysBetween += toDate.toDayOfYear().daysFromStartOfYear();

    // Count hole years between dates
    for (int i = fromDate.year + 1; i < toDate.year; i++) {
        if (i == 0) {  // No zero year
            continue;
        }
        if (DateHelper::isLeapYear(i)) {
            daysBetween += 366;
        } else {
            daysBetween += 365;
        }
    }

    return daysBetween;
}

QString DateHelper::getDayOfWeekLocalizedName(int dayOfWeekNum)
{
    switch(dayOfWeekNum) {
    case 1: return "Понедельник";
    case 2: return "Вторник";
    case 3: return "Среда";
    case 4: return "Четверг";
    case 5: return "Пятница";
    case 6: return "Суббота";
    case 7: return "Воскресение";
    }
    return "";
}

int DateHelper::getDaysCountInMonth(int month, bool isLeapYear)
{
    int days = 0;
    switch(month) {
    case 1:  days = 31; break;
    case 2:  days = 28; break;
    case 3:  days = 31; break;
    case 4:  days = 30; break;
    case 5:  days = 31; break;
    case 6:  days = 30; break;
    case 7:  days = 31; break;
    case 8:  days = 31; break;
    case 9:  days = 30; break;
    case 10: days = 31; break;
    case 11: days = 30; break;
    case 12: days = 31; break;
    }
    if(month == 2 && isLeapYear) {
        days++;
    }
    return days;
}

bool DateHelper::isLeapYear(int year)
{
    if (year == 0) {
        qDebug() << "Wrong year (zero).";
        return false;
    }

    if (year < 0) {
        year ++;
    }

    bool div4 = year % 4 == 0;
    bool div100 = year % 100 == 0;
    bool div400 = year % 400 == 0;

    if (div400)
        return true;

    if (div100)
        return false;

    if (div4)
        return true;

    return false;
}

Date::Date(int day, int month, int year)
{
    this->day = day;
    this->month = month;
    this->year = year;
}

Date::Date(QDate fromDate)
{
    this->day = fromDate.day();
    this->month = fromDate.month();
    this->year = fromDate.year();
}

DayOfYear Date::toDayOfYear() {
    DayOfYear ret;
    ret.day = day;
    ret.month = month;
    ret.isLeapYear = DateHelper::isLeapYear(year);
    return ret;
}

int DayOfYear::daysFromStartOfYear()
{
    int days = day - 1;

    for (int i = 1; i < month; i++) {
        days += DateHelper::getDaysCountInMonth(i, isLeapYear);
    }

    return days;
}

int DayOfYear::daysTillEndOfYear() {

    int days = DateHelper::getDaysCountInMonth(month, isLeapYear) - day;

    for (int i = month + 1; i <= 12; i++) {
        days += DateHelper::getDaysCountInMonth(i, isLeapYear);
    }

    return days;
}
